package src.Demo;

import src.Service.ILibraryService;
import src.Service.LibraryService;
import src.models.Book;
import src.models.Library;

import java.util.Arrays;

public class DemoService implements IDemoService{

    public void execute() {
        Library library = new Library();
        ILibraryService iLibraryService = new LibraryService();

        System.out.println("Размер библиотеки - 10 книг");
        System.out.println("Добавление 10 книг");
        iLibraryService.addBook(library, new Book("1984", "George Orwell") );
        iLibraryService.addBook(library, new Book("Metro 2033","Dmitry Glukhovsky"));
        iLibraryService.addBook(library, new Book("The Catcher in the Rye","Jerome Salinger"));
        iLibraryService.addBook(library, new Book("War and Peace","Leo Tolstoy"));
        iLibraryService.addBook(library, new Book("Fathers and Sons","Ivan Turgenev"));
        iLibraryService.addBook(library, new Book("The Cherry Orchard","Anton Chekhov"));
        iLibraryService.addBook(library, new Book("Metro 2034","Dmitry Glukhovsky"));
        iLibraryService.addBook(library, new Book("Papillon","Henri Charriere"));
        iLibraryService.addBook(library, new Book("The Cherry Orchard","Anton Chekhov"));
        iLibraryService.addBook(library, new Book("Romeo and Juliet","William Shakespeare"));
        iLibraryService.addBook(library, new Book("Crime and Punishment","Fyodor Dostoevsky"));

        System.out.println("Вывод всех добавленных книг");
        System.out.println(Arrays.toString(library.getBooks()) + "\n");

        System.out.println("Попытка добавления 11-ой книги");
        iLibraryService.addBook(library, new Book("The Epic Hunt for the Criminal Mastermind Behind the Silk Road ", "Nick Bilton"));
        System.out.println(Arrays.toString(library.getBooks())  + "\n");

        System.out.println("Поиск книги по тегу \"Dmitry GlukhovskY\"");
        System.out.println(Arrays.toString(iLibraryService.findBook(library, "Dmitry GlukhovskY"))+ "\n");

        System.out.println("Удаление книги \"Dmitry Glukhovsky\" - Metro 2033");
        iLibraryService.deleteBook(library, new Book("Metro 2033", "Dmitry Glukhovsky"));
        System.out.println(Arrays.toString(library.getBooks())  + "\n");

        System.out.println("Поиск книги по тегу \"Dmitry Glukhovsky\"");
        System.out.println(Arrays.toString(iLibraryService.findBook(library, "Dmitry Glukhovsky"))+ "\n");

        System.out.println("Поиск книги по тегу \"THE Cherry Orchard\"");
        System.out.println(Arrays.toString(iLibraryService.findBook(library, "THE Cherry Orchard"))+ "\n");


        System.out.println("Удаление одной книги \"Anton Chekhov\" - The Cherry Orchard");
        iLibraryService.deleteBook(library, new Book("The Cherry Orchard","Anton Chekhov"));
        System.out.println(Arrays.toString(library.getBooks())  + "\n");

        System.out.println("Поиск книги по тегу \"the cherry orchard\"");
        System.out.println(Arrays.toString(iLibraryService.findBook(library, "the cherry orchard"))+ "\n");

        System.out.println("Добавляем книгу");
        iLibraryService.addBook(library, new Book("The Epic Hunt for the Criminal Mastermind Behind the Silk Road ", "Nick Bilton"));
        System.out.println(Arrays.toString(library.getBooks())  + "\n");



    }
}
