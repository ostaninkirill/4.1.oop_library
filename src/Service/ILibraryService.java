package src.Service;

import src.models.Book;
import src.models.Library;

public interface ILibraryService {

    boolean addBook(Library library, Book book);
    boolean deleteBook(Library library, Book book);
    Book[] findBook (Library library, String search);



}
