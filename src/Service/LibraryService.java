package src.Service;

import src.models.Book;
import src.models.Library;

import java.util.ArrayList;
import java.util.List;

public class LibraryService implements ILibraryService {


    @Override
    public boolean addBook(Library library, Book book) {
        for (int i = 0; i < library.getBooks().length; i++) {
            if (library.getBooks()[i] == null) {
                library.getBooks()[i] = book;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean deleteBook(Library library, Book book) {
        for (int i = 0; i < library.getBooks().length; i++) {
            if (library.getBooks()[i] == null){
                continue;
            }
            if(library.getBooks()[i].equals(book)) {
                library.getBooks()[i] = null;
                return true;
            }
        }
        return false;
    }

    @Override
    public Book[] findBook(Library library, String search) {
        List<Book> bookList = new ArrayList<>();
        String searchLowerCase = search.toLowerCase();
        for (int i = 0; i < library.getBooks().length; i++) {
            if (library.getBooks()[i] == null){
                continue;
            }
            if (library.getBooks()[i].getBookAutour().toLowerCase().equals(searchLowerCase) || library.getBooks()[i].getBookTitle().toLowerCase().equals(searchLowerCase)){
                bookList.add(library.getBooks()[i]);
            }
        }
        return bookList.toArray(new Book[0]);
    }
}
