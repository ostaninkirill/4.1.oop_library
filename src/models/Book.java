package src.models;

import java.util.Objects;

public class Book {
    private String BookTitle;
    private String BookAutour;


    public Book(String bookTitle, String bookAutour) {
        BookTitle = bookTitle;
        BookAutour = bookAutour;
    }

    public String getBookTitle() {
        return BookTitle;
    }


    public String getBookAutour() {
        return BookAutour;
    }

    @Override
    public String toString() {
        return BookAutour + " - " + BookTitle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(BookTitle, book.BookTitle) &&
                Objects.equals(BookAutour, book.BookAutour);
    }

    @Override
    public int hashCode() {
        return Objects.hash(BookTitle, BookAutour);
    }
}
